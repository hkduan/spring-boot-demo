package com.louis.springboot.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "error desc of class")
@RestController
public class ErrorController {
    /* 方法注解 */
    @ApiOperation(value = "error desc of method", notes = "")
    @GetMapping(value="/error")
    public Object hello( /* 参数注解 */ @ApiParam(value = "desc of param" , required=false ) @RequestParam String name) {
        return "error " + name + "!";
    }
}
